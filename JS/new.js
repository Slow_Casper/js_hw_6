function createNewUser() {
    let firstName = prompt("Введіть ваше ім'я.");
    let lastName = prompt("Введіть ваше прізвище.");
    // let day = +prompt("Дата вашого народження");
    // let month = +prompt("Місяць вашого народження") - 1;
    // let year = +prompt("Рік вашого народження");
    let birthday = prompt("Вкажіть дату вашого народження в форматі mm.dd.yyyy");
    const birthDate = new Date(birthday);
    let now = new Date();

    const newUser = {
        set firstName(firstName) {
            this._firstName = firstName;
        },
        get firstName() {
            return this._firstName
        },
        set lastName(lastName) {
            this._lastName = lastName;
        },
        get lastName() {
            return this._lastName
        },
        getLogin() {
            return `${this.firstName.charAt(0).toLowerCase()}${this.lastName.toLowerCase()}`
        },
        getAge() {
            let age = now.getUTCFullYear() - birthDate.getUTCFullYear();
            let monthDiff = now.getUTCMonth() - birthDate.getUTCMonth();
            if (monthDiff !== 0) {
                return monthDiff < 0 ? (age - 1) : age;
            }

            let daysDiff = now.getUTCDate() - birthDate.getUTCDate();
            if (daysDiff !== 0) {
                return daysDiff < 0 ? (age - 1) : age;
            }

            return age
        },
        getPassword() {
            return `${this.firstName.charAt(0)}${this.lastName.toLowerCase()}${birthDate.getUTCFullYear()}`
        },
        
    };

    newUser.firstName = firstName;
    newUser.lastName = lastName;
    newUser.birthday = birthday;
    
    return newUser;
}

const user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());





// let now = Date.now();
// let day = +prompt("Дата вашого народження");
// let month = +prompt("Місяць вашого народження") - 1;
// let year = +prompt("Рік вашого народження");
// let birthday = new Date(year, month, day);
// let age = Math.floor((+now - +birthday) / (365 * 24 * 3600 * 1000));

// console.log(now);
// console.log(birthday);
// console.log(month);
// console.log(age);